# Men's Diving Watches Buying Tips - The Characteristics of a Diving Watch #

There are oodles of men's diving watches on the market, but how can you tell which ones are worth spending money on? And what precisely is a diving watch anyway?

A diving watch is a sports watch produced for under sea diving. Its essential purposes are to log your time below the water and to assist you in a safe return to sea level with the help of decompression tables (if an analog watch). Then there's the fact that many diving watches simply look terrific and can easily be worn as a fashion accessory when not used as an essential diving instrument.

A watch built for diving must be capable of withstanding water pressure equivalent to at least 100 meters deep, be rugged enough to withstand the corrosive sea water and shrug off an accidental blow or two. An authentic divers watch must meet a series of standards defined in ISO 6425, a world-wide standard that grants conforming watch manufacturers permission to imprint the words DIVER'S on the watch.

## Characteristics of a Diving Watch ##

Diving watches have a nominal level of functionality that must be fulfilled to abide by the ISO standard. Many watchmakers deliver additional features as well. Traditional dive watches were analog, but the emergence of diving watch computers has seen a digital variety of dive watch find more prominence in the market. So how exactly is one dive watch different from an ordinary wrist watch? There are several characteristics by which dive watches can be rated:

Water & Corrosion Resistive Watch Case

Due to the fact that diving watches must have sufficient water resistance, the watch cases are crafted from material like stainless steel, ceramics, titanium and plastics or synthetic resins. dive watches can also tolerate moderate levels of external magnetic disruption and shock. Even inbuilt movement of the more reputable dive watches employs smart impact protection.

Rotating Bezel (Elapsed Time Controller)

Keeping track of cumulative diving time is a crucial function of a dive watch. Analog watches feature a rotating bezel that addresses this. The bezel's function is to provide for easier registering of elapsed dive time. The bezel is turned to line up the zero on the bezel with the watch's second or minute hand, saving the diver the need to remember the original hand position and to perform the mental arithmetic needed to compute the total dive time. The bezel is one-way and can only be moved anti-clockwise to increase the perceived elapsed time (not reduce it). Some diving watches have a lockable bezel that reduces the risk of unintentional alteration underwater. Digital dive watches, of course just exhibit the duration of the dive in numeric form.

### Crystal Case ###

Due to the elevated force incurred underwater, diving watches tend to sport an ultra-thick crystal dial window. Some general materials use in dial windows include: synthetic sapphire, acrylic glass and hardened glass, each with their own set of advantages and disadvantages.

Acrylic glass is tolerant to shattering, but scratches easily
Hardened glass is more scratch tolerant than acrylic glass but less brittle than sapphire
Sapphire is very tolerant to scratching, but will break much more easily than the other materials.
Many watch designers use combinations of these basic materials.
Crown

Virtually all analog diving watches feature a water-proof crown. The crown must usually be unscrewed to set or correct the time or date and screwed in again to restore water resistance.

### Helium Release Valve ### 

Most dive watches are created for "shallow" dives, no deeper than 200 meters beneath sea level. Others are designed to go 1000's of meters deep. Diving to this level is known as "saturation diving" or "technical diving". A problem encountered in ultra-deep saturation dives which are performed in Helium rich environments is pressure build-up caused by helium getting into the watch. Without a proper venting mechanism, the crystal dial cases would often shoot off due to the pressure buildup of helium within the interior. Manufacturers of saturation dive watches compensated for this by installing release valves to expel the excess internal gas.

Strap/Bracelet

Most diving watches have a rubber, silicone or polyurethane strap or a metal watch bracelet with excess length to permit wearing the watch over the sleeve of a diving suit. Watchstraps often have a concealed extension deployment buckle by which it can be appropriately extended.

### Readability ###

Dive watches must be legible in the low light environment experienced deep below the sea surface. ISO 6425 mandates that the watch must features an indicator of operation in the dark. Most dive watches feature high contrasting, non-cluttered dials with distinctly marked numerals, minute marks and hands, typically laced with a coat of radiant pigmentation.

### Power Reserve Indicator ###

If a dive watch is powered by a battery, ISO 6425 requires that it exhibit an End Of Life (EOL) indicator to warn of an low energy reserve. This is usually handled with a two or four second skipping of the second hand or a cautionary message on a digital watch.

While a good diving watch may appear expensive, keep in mind that with these types of watches, you are obtaining an ultra-durable timepiece that has been put through a set of much more arduous tests than a standard sports watch. Men's diving watches can take a hit or two and a good one will last near a lifetime. Of course if you're a serious diver, you really can't live without one.

* [https://arnoldandbecker.com/](https://arnoldandbecker.com/)

